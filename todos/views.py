from django.shortcuts import render, get_list_or_404
from todos.models import TodoList, TodoItem


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    tasks = get_object_or_404(TodoList, id=id)
    context = {"tasks": tasks}
    return render(request, "todos/detail.html", context)
